### Extras for Aedificium
[Lua](https://lua.org)-based plugin for [Cuberite](https://cuberite.org) which provides fun commands and nonessentials for the leisure of players.
<br>
Based off the [mathiascode/extras](https://github.com/mathiascode/Extras) plugin by [Mathias](https://github.com/mathiascode).

### Visitor commands
By default server configuration, these commands are granted to visitors.
| Usage | Permission(s) | Description |
| ------- | ---------- | ----------- |
| `/name <name>` | `extras.name` | Changes your name on the server. |

### Architect commands
By default server configuration, these commands are granted to architects.
| Usage | Permission(s) | Description |
| ------- | ---------- | ----------- |
| `/disguise <disguise> <entity[:baby]> [name]` | `extras.disguise` | Disguises you as any entity. |
| `/draw <address>` | `extras.draw` | Draws an image on a selected pre-generated map. |
| `/reveal` | `extras.reveal` | Reveals you of any disguise. |
| `/scare <player>` | `extras.scare` | Gives any player quite the scare. |
