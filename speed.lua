-- speed.lua
-- Implements the /speed command.

function HandleSpeedCommand(Split, Player)
    if (Split[2] ~= 2) then
        SendMessage("Usage: /speed <speed>")
        return true
    end

    local Speed = tonumber(Split[2])
    if not(Speed) then
        SendMessage("Couldn't set a non-numeric speed.")
        return true
    end

    if (Speed > 100) then
        SendMessage("Couldn't set a speed over the limit of 100.")
        return true
    end

    Player:SetFlyingMaxSpeed(Speed)
    Player:SetNormalMaxSpeed(Speed)
    Player:SetSprintingMaxSpeed(Speed)
    Player:SendMessage("Set your maximum speed to " .. Speed .. ".")
    return true
end
